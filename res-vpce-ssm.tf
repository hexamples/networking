resource "aws_vpc_endpoint" "ssm" {
  # Estas cosas cuestan: con 4 bastan para igualar el costo de un NAT GW
  for_each = var.enable_ssm_vpce ? toset([
    "com.amazonaws.${data.aws_region.current.name}.ssm",
    "com.amazonaws.${data.aws_region.current.name}.ec2messages",
    "com.amazonaws.${data.aws_region.current.name}.ec2",
    "com.amazonaws.${data.aws_region.current.name}.ssmmessages",
  ]) : toset([])

  vpc_id              = module.vpc.vpc_id
  vpc_endpoint_type   = "Interface"
  service_name        = each.key
  subnet_ids          = module.vpc.private_subnets
  security_group_ids  = [module.ssm_sg[0].security_group_id]
  private_dns_enabled = true

  tags = local.tags
}

module "ssm_sg" {
  count = var.enable_ssm_vpce ? 1 : 0

  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4"

  name        = "${var.scope_name}-ssm-sg"
  description = "Allow all traffic from private subnets"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    for cidr_block in module.vpc.private_subnets_cidr_blocks: {
      from_port   = 0
      to_port     = 0
      protocol    = -1
      description = "All protocols"
      cidr_blocks = cidr_block
    }
  ]
  
  egress_rules = ["all-all"]

  tags = local.tags
}
