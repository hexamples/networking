# networking
Configuración más simple de redes en AWS.<br>
Se hace uso del módulo [terraform-aws-modules/vpc/aws](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest)

## Extras
Se muestra opcionalmente la configuración de vpc endpoints para habilitar el uso de AWS Session Manager en una red privada sin necesidad de NAT Gateway.
