variable "scope_name" { default = "" }
variable "tags" { default = {} }

variable "vpc_cidr_prefix" {}
variable "enable_ssm_vpce" { default = false }
