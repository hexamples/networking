module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.1"

  name            = var.scope_name
  cidr            = "${var.vpc_cidr_prefix}.0.0/16"
  azs             = ["${data.aws_region.current.name}a", "${data.aws_region.current.name}b"]
  public_subnets  = ["${var.vpc_cidr_prefix}.0.0/22", "${var.vpc_cidr_prefix}.4.0/22"]
  private_subnets = ["${var.vpc_cidr_prefix}.100.0/22", "${var.vpc_cidr_prefix}.104.0/22"]

  enable_nat_gateway     = false
  single_nat_gateway     = false
  one_nat_gateway_per_az = false

  create_database_subnet_group           = true
  create_database_subnet_route_table     = true
  create_database_internet_gateway_route = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = local.tags
}
